using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen2194309.Models;
using Examen2194309.Controllers;
using Microsoft.AspNetCore.Mvc;


namespace Examen2194309Tests
{
    // TODO: Tu devrais, idéalement, ranger ta classe dans un 
    //       dossier Controllers (car ce sont des tests pour 
    //       les contrôleurs)) et il faudrait une classe de 
    //       tests pour chaque contrôleur.
    public class ControllersArticleShould
    {

        //TODO: Tes tests ne sont pas complet dans toutes les méthodes.
        
        [Fact]
        public void AfficherIndexTest()
        {
            var acceuilController = new AcceuilController();

            var result = acceuilController.Index();

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void AfficherTotalSouhaitsTest()
        {
            var ArticlesController = new ArticleController();
            var result = ArticlesController.TotalSouhaits() as ViewResult;
            Assert.Equal("Tout les souhaits", result.ViewData["Titre"]);
        }

        [Fact]
        public void AfficherMesSouhaitsTest()
        {
            var ArticlesController = new ArticleController();
            var result = ArticlesController.MesSouhaits() as ViewResult;
            Assert.Equal("Mes listes de souhaits", result.ViewData["Titre"]);

        }

        [Fact]
        public void AfficherSouhaitsTest()
        {
            var ArticlesController = new ArticleController();
            var result = ArticlesController.Souhaits(2) as ViewResult;
            Assert.Equal("Souhaits de la liste choisie", result.ViewData["Titre"]);
            Assert.NotEqual(2, result.ViewData["id"]);
        }

    }
}
