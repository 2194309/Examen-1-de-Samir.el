﻿namespace Examen2194309.Models
{
    public class Article
    {
        public int Id { get; set; } 
        public string Nom { get; set; }


        public Article(int id, string nom)
        {
            Id = id;
            Nom = nom;
        }
    }
}
