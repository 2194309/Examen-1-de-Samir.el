using Microsoft.AspNetCore.Mvc;
using Examen2194309.Models;

namespace Examen2194309.Controllers
{
    public class ArticleController : Controller
    {
        private readonly List<Article> _articles;
        public ArticleController()
        {
            _articles = new List<Article>()
            {
                // TODO: les noms de tes articles ne sont pas supposés être les mêmes. 
                //       Je comprend que tu t'en serves pour les afficher facilement 
                //       dans une vue mais ce n'est pas bon. 
                //       Tu as contourné le problème en arrangeant les données à ton programme, 
                //       mais c'est un peu de la triche. Tu devrais penser à une structure de 
                //       données différente qui te permettre de résoudre le vrai problème réél.
                new Article(0, "Sport"),
                new Article(1, "Sport"),
                new Article(2, "Sport"),
                new Article(3, "Sport"),
                new Article(4, "Jeu"),
                new Article(5, "Jeu"),
                new Article(6, "Jeu"),
                new Article(7, "Jeu"),
                new Article(8, "Outil"),
                new Article(9, "Outil"),
                new Article(10, "Outil"),
                new Article(11, "Outil"),
                new Article(12, "Vetements"),
                new Article(13, "Vetements"),
                new Article(14, "Vetements"),
                new Article(15, "Vetements"),
            };
        }

        // TODO: Cette méthode d'action ne devrait pas exister, 
        //       tu devais réutiliser la méthode d'action Souhaits.
        public ViewResult TotalSouhaits()
        {

            ViewBag.Titre = "Tout les souhaits";

            return View(_articles);
        }


        public ViewResult MesSouhaits()
        {

            ViewBag.Titre = "Mes listes de souhaits";

            // TODO: Tu devais envoyer les donnes (liste des souhaits) 
            //       à ta vue à partir d'ici (ton contrôleur). 
            return View("MesSouhaits");
        }


        // TODO: Tu devais réutiliser cette méthode pour
        //       afficher la liste de tous les articles.
        public ViewResult Souhaits(int id)
        {
            // TODO: Tu te sert du nom de l'article pour 
            //      sélectionner les articles à afficher. 
            //      Ta solution ne fonctionnera pas en 
            //      réalité car les noms des articles 
            //      ne sont pas supposés être les mêmes.
            int idArticle = _articles.FindIndex(i => i.Id == id);

            string nomArticle = _articles[idArticle].Nom;

            ViewBag.Titre = "Souhaits de la liste choisie";
            ViewData["nomArticle"] = nomArticle;

            return View(_articles);
        }
    }
}
